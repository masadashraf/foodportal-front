(function ($) {
    "use strict";

    /*-----------------------------------------------------------------------------------*/
    /* Advance Select-ables
     /*-----------------------------------------------------------------------------------*/
    if (jQuery().selectric) {
        $( ".advance-selectable" ).selectric();
    }


    /*-----------------------------------------------------------------------------------*/
    /* home main slider variation three
     /*-----------------------------------------------------------------------------------*/
    if (jQuery().slick) {

        // Slider Home Recipe
        $('.home-recipe-slider').slick({
            autoplay: true,
			autoplaySpeed: 3000,
            infinite: true,
            fade: true,
            pauseOnHover: false,
            dots: true
        });
		
		// Slider Home Round Small Categories
        $('.home-recipes-categories-round').slick({
            slidesToShow: 10,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 2000,
			infinite: true,
			swipeToSlide: true,
            responsive: [
                {
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: 8

                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 6

                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 5

                    }
                },
                {
                    breakpoint: 450,
                    settings: {
                        slidesToShow: 3

                    }
                }
            ]
        });
		
		// Slider Home Featured Recipes
        $('.Latest-Recipes').slick({
            slidesToShow: 4,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 5000,
			infinite: true,
			swipeToSlide: true,
            responsive: [
                {
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: 3

                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2

                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2

                    }
                },
                {
                    breakpoint: 450,
                    settings: {
                        slidesToShow: 1

                    }
                }
            ]
        });
		
		
        $('.Banner-Inner-Recipe').slick({
            slidesToShow: 3,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 2000,
			infinite: true,
			swipeToSlide: true,
            responsive: [
                {
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: 3

                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2

                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2

                    }
                },
                {
                    breakpoint: 450,
                    settings: {
                        slidesToShow: 1

                    }
                }
            ]
        });
		
		
		
		// Slider Home Master Techniques
        $('.Master-The-Techniques').slick({
            slidesToShow: 4,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 5000,
			infinite: true,
			swipeToSlide: true,
            responsive: [
                {
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: 3

                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2

                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2

                    }
                },
                {
                    breakpoint: 450,
                    settings: {
                        slidesToShow: 1

                    }
                }
            ]
        });



        // Recipe slider inner recipe detail page
        $('.slider-detail2').slick({
            autoplay: true,
            infinite: true,
            pauseOnHover: true,
            prevArrow: $('.left-arrow'),
            nextArrow: $('.right-arrow')
        });



        // unslick on print

        $(".print-button").on('click', function(e){

            $(".steps-tabs").slick('unslick');
            $(".steps-detail").slick('unslick');
            window.print();
            e.preventDefault();
        } );
    }




    /*-----------------------------------------------------------------------------------*/
    /* Mean Menu
    /*-----------------------------------------------------------------------------------*/
    function menuDrop(target){
        var mainMenuItem = $(target);
        mainMenuItem.on( 'mouseenter',
            function () {
                $(this).children('ul').slideDown(200);
            });

        mainMenuItem.on( 'mouseleave',
            function () {
                $(this).children('ul').stop(true).slideUp(200);
            }
        );
    }

    menuDrop(".top-nav ul li");
    menuDrop(".nav-collapse ul li");


    if($(".header").hasClass("header-var1")){
        var navList = $(".main-menu.left").html();
        navList += $(".main-menu.right").html();


        $('.responsive-menu').html("<ul>" + navList + "</ul>");
    }

    if (jQuery().meanmenu) {
        $('.responsive-menu').meanmenu({
            meanScreenWidth: "991"
        });
    }



   


    /*-----------------------------------------------------------------------------------*/
    /* Tabs
    /*-----------------------------------------------------------------------------------*/
    var $tabsNav = $('.tabs-nav'),
        $tabsNavLis = $tabsNav.children('li');

    $tabsNav.each(function () {
        var $this = $(this);
        $this.next().children('.tab-content').stop(true, true).hide()
            .first().show();
        $this.children('li').first().addClass('active').stop(true, true).show();
    });

    $tabsNavLis.on('click', function (e) {
        var $this = $(this);
        $this.siblings().removeClass('active').end()
            .addClass('active');
        var idx = $this.parent().children().index($this);
        $this.parent().next().children('.tab-content').stop(true, true).hide().eq(idx).fadeIn();
        e.preventDefault();
    });


    /*-----------------------------------------------------------------------------------*/
    /*	Accordion
    /*-----------------------------------------------------------------------------------*/
    
	
	jQuery('dl.accordion dt').on('click', function() {
		  
	  if( $(this).hasClass('current')){
		   $(this).parent().children('dd').slideUp();	
		  jQuery('dl.accordion dt').removeClass('current');
		   
	  } else {
		  
		 $(this).siblings('dt').removeClass('current');
         $(this).addClass('current').next('dd').stop(true, true).slideDown(500).siblings('dd').stop(true, true).slideUp(500);
	  }
	return false;
	  });



    /*-----------------------------------------------------------------------------------*/
    /*	Animation CSS integrated with wow.js Plugin
     /*----------------------------------------------------------------------------------*/

    new WOW().init({ });

    $(function (){
        if (!$(".footer").hasClass("animate-footer")) {
            $(".footer").find(".wow").addClass("disable-wow");
        }
    });

    /*-----------------------------------------------------------------------------------*/
    /* swipebox
    /*-----------------------------------------------------------------------------------*/
    if (jQuery().swipebox) {
        $('.swipebox').swipebox();
		$('.swipebox-video').swipebox();
    }


})(jQuery);


$(document).ready(function () {
   $('.search-trigger, .mobile-menu-search-toggler').on('click', function(){
      $('body').addClass('active-search-form');
	  $('.main-search-container').fadeIn(1);
      $('.main-search-form-overlay').fadeIn(300);
      $('.main-search-form .search-field').focus();
    });
	
	$('.main-search-form-overlay, .meanmenu-reveal').on('click', function(){
      $('body').removeClass('active-search-form');
      $('.main-search-form-overlay').fadeOut(300);
	  $('.main-search-container').fadeOut(500);
    });
	
	$(document).keyup(function(e) {
      if (e.keyCode == 27) { 
        $('body').removeClass('active-search-form');
        $('.main-search-form-overlay').fadeOut(300);
		$('.main-search-container').fadeOut(500);
      }
    });
});
