module.exports = {
  megaMenuSlider: function () {
    $('#mgmenu1').universalMegaMenu({
      menu_effect: 'hover_fade',
      menu_speed_show: 300,
      menu_speed_hide: 200,
      menu_speed_delay: 200,
      menu_click_outside: false,
      menubar_trigger : false,
      menubar_hide : false,
      menu_responsive: true
    });
    megaMenuContactForm();
  },

  homeSliderSlick: function () {
    $('.home-recipe-slider').slick({
      autoplay: true,
      autoplaySpeed: 3000,
      infinite: true,
      fade: true,
      pauseOnHover: false,
      dots: true
    });
  },

  homeRecipesCategoriesRound: function() {

    $('.home-recipes-categories-round').slick({
      slidesToShow: 10,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      infinite: true,
      swipeToSlide: true,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 8

          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 6

          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 5

          }
        },
        {
          breakpoint: 450,
          settings: {
            slidesToShow: 3

          }
        }
      ]
    });

  },
  // Slider Home Round Small Categories

    latestRecipes: function() {
      $('.Latest-Recipes').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        infinite: true,
        swipeToSlide: true,
        responsive: [
          {
            breakpoint: 1199,
            settings: {
              slidesToShow: 3

            }
          },
          {
            breakpoint: 991,
            settings: {
              slidesToShow: 2

            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 2

            }
          },
          {
            breakpoint: 450,
            settings: {
              slidesToShow: 1

            }
          }
        ]
      });
    },

  masterTheTechniques: function () {
    // Slider Home Master Techniques
    $('.Master-The-Techniques').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 5000,
      infinite: true,
      swipeToSlide: true,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 3

          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2

          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2

          }
        },
        {
          breakpoint: 450,
          settings: {
            slidesToShow: 1

          }
        }
      ]
    });
  }

};
