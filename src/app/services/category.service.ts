import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {IResponse} from '../interface/IResponse';
import {Constants} from '../constants';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  getCategories() {
    return this.http.get<IResponse<IListing.ParentCategory[]>>(`${Constants.BASE_API_URL}/header/menu`);
  }
}

