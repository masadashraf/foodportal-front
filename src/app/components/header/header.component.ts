import {Component, OnInit, AfterViewInit } from '@angular/core';
import {CategoryService} from '../../services/category.service';
import {megaMenuSlider} from '../../../assets/js/sliders';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit , AfterViewInit {

  public categories;

  constructor(private _categoriesService: CategoryService) {
  }

  ngOnInit(): void {
    this.getCategories();
  }

  getCategories() {
    return this._categoriesService.getCategories().subscribe(
      data => {
        console.log(data.Result);
        this.categories = data.Result;
      },
      err => console.log()
    );
  }

  ngAfterViewInit(): void {
    megaMenuSlider();
  }
}
