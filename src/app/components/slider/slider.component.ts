import {AfterContentInit, Component, OnInit} from '@angular/core';
import {homeRecipesCategoriesRound} from '../../../assets/js/sliders';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit, AfterContentInit  {

  constructor() { }

  ngOnInit() {
  }

  ngAfterContentInit(): void {
  homeRecipesCategoriesRound();
  }

}
