import {AfterContentInit, Component, OnInit} from '@angular/core';
import {latestRecipes} from '../../../assets/js/sliders';

@Component({
  selector: 'app-featured-recipes',
  templateUrl: './featured-recipes.component.html',
  styleUrls: ['./featured-recipes.component.css']
})
export class FeaturedRecipesComponent implements OnInit, AfterContentInit {

  constructor() { }

  ngOnInit() {
  }
  ngAfterContentInit(): void {
    latestRecipes();
  }

}
