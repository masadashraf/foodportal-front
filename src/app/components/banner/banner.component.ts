import { Component, OnInit, AfterContentInit } from '@angular/core';
import {homeSliderSlick} from '../../../assets/js/sliders';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit, AfterContentInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterContentInit(): void {
    homeSliderSlick();
  }

}
