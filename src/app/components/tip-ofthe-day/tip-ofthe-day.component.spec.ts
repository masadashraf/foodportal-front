import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipOftheDayComponent } from './tip-ofthe-day.component';

describe('TipOftheDayComponent', () => {
  let component: TipOftheDayComponent;
  let fixture: ComponentFixture<TipOftheDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipOftheDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipOftheDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
