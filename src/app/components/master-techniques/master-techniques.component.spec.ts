import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterTechniquesComponent } from './master-techniques.component';

describe('MasterTechniquesComponent', () => {
  let component: MasterTechniquesComponent;
  let fixture: ComponentFixture<MasterTechniquesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterTechniquesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterTechniquesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
