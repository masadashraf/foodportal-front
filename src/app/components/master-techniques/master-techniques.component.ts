import {AfterContentInit, Component, OnInit} from '@angular/core';
import {masterTheTechniques} from '../../../assets/js/sliders';

@Component({
  selector: 'app-master-techniques',
  templateUrl: './master-techniques.component.html',
  styleUrls: ['./master-techniques.component.css']
})
export class MasterTechniquesComponent implements OnInit, AfterContentInit {

  constructor() { }

  ngOnInit() {
  }
  ngAfterContentInit(): void {
    masterTheTechniques();
  }

}
