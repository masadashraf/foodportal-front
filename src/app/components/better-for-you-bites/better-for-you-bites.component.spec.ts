import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BetterForYouBitesComponent } from './better-for-you-bites.component';

describe('BetterForYouBitesComponent', () => {
  let component: BetterForYouBitesComponent;
  let fixture: ComponentFixture<BetterForYouBitesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BetterForYouBitesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BetterForYouBitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
