declare module IListing {

  export interface ParentCategory {
    id: number;
    title_en: string;
    title_ur: string;
    slug: string;
    description_en: string;
    description_ur: string;
    categories: Category;
  }

  export interface Category {
    id: number;
    category_title_ur: string;
    category_title_en: string;
    category_slug: string;
    feature_type_id: string;
    parent_id: number;
    categories: Category;
  }
}
