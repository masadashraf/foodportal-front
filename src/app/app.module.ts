import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { BannerComponent } from './components/banner/banner.component';
import { SliderComponent } from './components/slider/slider.component';
import { PopularRecipesComponent } from './components/popular-recipes/popular-recipes.component';
import { FeaturedRecipesComponent } from './components/featured-recipes/featured-recipes.component';
import { TipOftheDayComponent } from './components/tip-ofthe-day/tip-ofthe-day.component';
import { NewsletterComponent } from './components/newsletter/newsletter.component';
import { BetterForYouBitesComponent } from './components/better-for-you-bites/better-for-you-bites.component';
import { MasterTechniquesComponent } from './components/master-techniques/master-techniques.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    BannerComponent,
    SliderComponent,
    PopularRecipesComponent,
    FeaturedRecipesComponent,
    TipOftheDayComponent,
    NewsletterComponent,
    BetterForYouBitesComponent,
    MasterTechniquesComponent
  ],
  imports: [
    BrowserModule,
      AppRoutingModule,
      HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
